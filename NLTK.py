#!/usr/bin/python3

# Unused functions that could be used later

from nltk import word_tokenize
from langdetect import detect
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer

languages = {
    "ar": 	"arabic",
    "da": 	"danish",
    "en": 	"english",
    "fi": 	"finnish",
    "fr": 	"french",
    "de": 	"german",
    "el": 	"greek",
    "hu": 	"hungarian",
    "it": 	"italian",
    "no": 	"norwegian",
    "pt": 	"portuguese",
    "ro": 	"romanian",
    "ru": 	"russian",
    "es": 	"spanish",
    "sv": 	"swedish"
}

"""
# List of words that identify an attribute as necessary.
#RequireModList = ['able', 'require', 'need', 'necessary', 'purposes']

# List of words that mark an attribute as optional.
# OptionalModList = [''may]

# List of what an attribute could be used for.
#InternalUsageListe = ['research', 'operate', 'analyze', 'improve', 'optimize', 'process', 'store']

#
#ExternalUsageListe = ['']

# List of words that negate a statement.
#UnuseList = ['not', "won't",  ]

# List of words that cancel a negation.
#ReuseList = ['but']
"""

def drop_stopwords(words, lang):
    '''Recognize the Language and drop the stopwords.

    First, all single words must be sourced. Then the language is
    recognized from the raw data. Finally, the stop words of the
    respective language are filtered from the text.
    '''
    stop_words = set(stopwords.words(languages[lang]))
    filtered_sentence = []
    for w in words:
        if str(w).lower() not in stop_words:
            filtered_sentence.append(w)
    return filtered_sentence


def stem_words(words):
    '''Shorten all words to their stems'''
    ps = PorterStemmer()
    stemmed = []
    for w in words:
        stemmed.append(ps.stem(w))
    return stemmed


# Detect the language
raw = ""
lang = detect(raw)
# Get the Words
words = word_tokenize(raw)
# Drop all Stopwords
words = drop_stopwords(words, lang)
# Stem all words
words = stem_words(words)
