#!/usr/bin/python3

from os import path, getlogin, makedirs
import sys
import re
from shutil import copy
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from pandas import DataFrame, Series
from nltk import sent_tokenize
from nltk.stem import SnowballStemmer
from spacy import displacy
from collections import Counter
import en_core_web_sm
import click
from pprint import pprint

# List of attributes
ATTRIBUTE_LIST = []

# Cache Directory for HTML and CSV Files
CacheDir = "/home/" + getlogin() + "/.cache/PrivacyPolicyScraper/"

# Config Directory for Attribute List
ConfDir = "/home/" + getlogin() + "/.config/PrivacyPolicyScraper/"


def preload():
    ''' In this function, the cache and the config folder are created
    and the attributes are read '''
    # Create a config directory if it does not exist
    if not path.exists(ConfDir):
        makedirs(ConfDir)

    # If the Configuration does not exist, copy the attribute_list
    if not path.exists(ConfDir + 'attribute_list'):
        copy('./attribute_list', ConfDir + 'attribute_list')

    # Create a cache directory if it does not exist
    if not path.exists(CacheDir):
        makedirs(CacheDir)

    # Read the attribute list
    with open(ConfDir + "attribute_list", encoding='UTF-8') as file:
        content = ""
        for line in file:
            line = line.split('#', 1)[0].rstrip()
            line = re.sub(r'\s?"', '', line)
            content += line
    global ATTRIBUTE_LIST
    ATTRIBUTE_LIST = content.split(",")


def fetch_soup(url, pagename):
    '''Get the HTML source of a page.

    Check if the URL has already been scraped, otherwise get the
    HTML source from the page
    '''
    if path.exists(CacheDir + pagename + '.html'):
        with open(CacheDir + pagename + '.html', encoding='utf-8') as file:
            content = file.read()
            soup = BeautifulSoup(content, 'html5lib')
    else:
        print("Soup not found, fetching it...")
        print("Fetching: " + url)
        hdr = {'User-Agent': 'Mozilla/5.0'}
        req = Request(url, headers=hdr)
        with urlopen(req) as res:
            page = res.read()
        soup = BeautifulSoup(page, "html5lib")
        with open(CacheDir + pagename + ".html", "w", encoding='UTF-8') as html_file:
            html_file.write(str(soup.prettify()))
    return soup


def crawl(soup):
    ''' Get all links of current page and return the prioritized one containing privacy '''
    strings = soup.find_all('a')
    if not strings:
        site = input(
            "The privacy statement was not found, please enter the URL: ")
        if not "http" in site or "www" in site:
            print("That doesn't appear to be a URL!")
            sys.exit()
    for link in strings:
        ref = link.get("href")
        if ref not in ["", None]:
            for subsite in ["/legal/privacy-policy", "/privacy", "privacy"]:
                if subsite in ref:
                    site = ref
    return site


def fetch_raw(soup):
    '''Obtain the raw text of a page'''
    raw = ""
    for para in soup.find_all("p"):
        raw += para.get_text()
    return raw


def fetch_paragraph(soup, heading):
    '''Obtain a paragraph of a page based on a string in the heading'''
    paragraph = None
    # Search all Paragraphs based on headings
    for header in soup.find_all(re.compile(r'h\d+')):
        if heading in header.text and "from" not in header.text:
            paragraph = header.text
            while next_sibling := header.find_next_sibling(['p', re.compile(r'h\d+')]):
                if re.match(r'h\d+', next_sibling.name):
                    return paragraph
                paragraph += next_sibling.text
                header = header.find_next_sibling(
                    ['p', re.compile(r'h\d+')])
    # Search all Paragraphs based on tables
    curr = ""
    tables = soup.find_all("table")
    for table in tables:
        table_body = table.find('tbody')
        rows = table_body.find_all('tr')
        for row in rows:
            try:
                curr = row.find('td').find('u')
                if curr is not None:
                    curr = curr.text.strip()
                    if heading in curr and "from" not in header.text:
                        paragraph = row.find_all('td')
                        paragraph = [x.text.strip() for x in paragraph]
                        paragraph = [re.sub(r'\n', '', x) for x in paragraph]
                        return ''.join(paragraph)
            except ValueError:
                continue


def collect(sentences):
    '''Find all sentences that contain keywords.

    Search each sentence for each keyword of a previously defined list
    "sentences". Store all values in a series and merge them into a
    dataframe.
    '''
    df = DataFrame()
    for word in ATTRIBUTE_LIST:
        collection = Series(
            [sentence for sentence in sentences if word in sentence], name=word, dtype='object')
        df[word] = collection
    return df


def to_csv(collections, pagename):
    '''Store Sentences in a CSV'''
    collections.dropna(how='all', axis=1, inplace=True)
    print("Schreibe in Datei " + pagename + '_collections.csv')
    collections.T.to_csv(pagename + '_collections.csv')


def to_html(collections, pagename):
    '''Store Sentences in a HTML'''
    collections.dropna(how='all', axis=1, inplace=True)
    print("Schreibe in Datei " + pagename + '_collections.html')
    collections.T.to_html(pagename + '_collections.html')


def get_mentions(collections):
    '''Check which Columns are empty'''
    occurrence_list = [col for col in collections.columns if
                       not collections[col].isnull().all()]
    return occurrence_list


def check_col(collections, cols):
    '''Return if a column or a list of columns is empty'''
    if isinstance(cols, str):
        value = cols in get_mentions(collections)
        return value
    if isinstance(cols, list):
        values = []
        for col in cols:
            values.append(col in get_mentions(collections))
        return values
    return None


def bold(text):
    '''Make a string bold'''
    return "\033[1m" + text + "\033[0m"


def extract_ne(quote):
    '''Extract all NEs of a text'''
    nlp = en_core_web_sm.load()
    doc = nlp(quote)
    return ([(X.text, X.label_) for X in doc.ents])


def stem_words(words):
    '''Shorten all words to their stems'''
    sno = SnowballStemmer('english')
    stemmed = []
    if isinstance(words, list):
        for w in words:
            stemmed.append(sno.stem(w))
    elif isinstance(words, str):
        stemmed = sno.stem(words)
    return stemmed


@ click.command()
@ click.argument('url', type=click.STRING)
@ click.option('--attributes', '-a', is_flag=True,
               help="Print all keywords which are searched.")
@ click.option('--long', '-l', is_flag=True,
               help="Print all keywords and the sentences in which they are found.")
@ click.option('--short', '-s', is_flag=True,
               help="Print all the keywords that appear in the text.")
@ click.option('--export', '-e', multiple=True,
               help="Print all keywords and the sentences in which they are \
              found to a csv/html file.")
@ click.option('--parties', '-p', is_flag=True,
               help="List all parties to which information is shared.")
def run(url, attributes, long, short, export,  parties):
    '''This is the central function for the program flow.

    Here a command line parameter is used to get the URL. First, the
    cache directory is created if it does not already exist. Then the
    name of the page is obtained from the URL. Then the HTML source
    code and the text in it is stored in variables. Then sentences are
    filtered and these are checked for keywords.
    '''

    # Print the attribute_list
    if attributes:
        print(ATTRIBUTE_LIST)
        sys.exit()

    # Fetch the Name of the Webpage
    pagename = re.findall(r"(?<=:\/\/).*?(?=\/)", url)[0]
    # Fetch the Sources
    soup = fetch_soup(url, pagename)

    # Fetch the Privacy Policy Website
    if "privacy" not in url:
        ppurl = crawl(soup)
        if ppurl is None:
            print("Error loading privacy policy")
            sys.exit()
        if pagename not in ppurl:
            if "https" not in ppurl and "www" not in ppurl:
                ppurl = "https://" + pagename + ppurl
        soup = fetch_soup(ppurl, pagename + "_policy")

    # Fetch the Raw Text
    raw = fetch_raw(soup).replace("\n", "")
    raw = re.sub(r'\s+', ' ', raw)
    # Get all the Sentences
    sentences = sent_tokenize(raw)
    # Fetch the Sentences by Category
    collections = collect(sentences)
    collections.dropna(how='all', axis=1, inplace=True)
    # Filter Duplicates
    last = ""
    try:
        for col in collections:
            if last == collections[col].values.all():
                collections.drop(col, axis=1, inplace=True)
                last = collections[col].values.all()
    except ValueError:
        pass

    if export:
        if "".join(export) == "csv":
            # Write the collected Sentences to a CSV file
            to_csv(collections, pagename)
        elif "".join(export) == "html":
            # Write the collected Sentences to a HTML file
            to_html(collections, pagename)

    if short:
        # Print the collected Words
        print(pagename + " collects the following Information: \n")
        for col in collections:
            print(str(col).capitalize())
    elif long:
        # Print the collected Sentences
        print(pagename + " collects the following Information: \n")
        for col in collections:
            line = str(collections[col].values)
            line = re.sub("nan", "", line)
            line = re.sub(r"\[[\'\"]", "", line)
            line = re.sub(r"[\'\"](\n)*( )*[\'\"]", "\n\n", line)
            line = re.sub(r"[\'\"]( )*(\n)*( )*]", "\n", line)
            print(bold(str(col).capitalize()) + ": \n" + line + "\n")

    if parties:
        # Get the paragprah containing "share", as in "How we share your information"
        for heading in ["share", "Third"]:
            paragraph = fetch_paragraph(soup, heading)
            if paragraph is not None:
                break

        # Obtain the NEs that include organization, government, and work of art
        named_entities = extract_ne(paragraph)
        orgs = []
        exceptions = ['Third', 'Others', 'Affiliated',
                      'Services', 'API', 'Pages', 'Social', 'IP', "Policy"]
        for pair in named_entities:
            if pair[1] in ["ORG", "GPE", "WORK_OF_ART"]:
                # Filter out all duplicates, plural words and similar,
                # as well as common non-relevant words
                if pair[0] not in orgs and ' '.join(pair[0].split(" ")[:-1]) not in [' '.join(org.split(" ")[:-1]) for org in orgs] and stem_words(pair[0]) not in stem_words(exceptions):
                    found = False
                    for e in exceptions:
                        if e in pair[0]:
                            found = True
                            break
                    if not found:
                        orgs.append(pair[0])
        if len(orgs) == 0:
            print("No third parties receive information from the Service")
        else:
            print(orgs)


if __name__ == '__main__':
    # Create the cache and the config folder and read the attributes
    preload()

    # Initialize the program with the command line parameters
    run()
