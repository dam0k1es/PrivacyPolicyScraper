# ABOUT

This tool can be used to search website privacy statements for
keywords. They can be printed on their own or in the sentence in which
they appear. Third-party providers may also be printed who receive
information from the website.

The keywords are in a file ~/.config/PrivacyScraper/attribute_list and
can be customized as you wish.

The project is mainly intended for learning, but already saves some
time when evaluating a website. **I take no responsibility for improper
use!**

# PROCEDURE
1. Read a privacy policy every once in a while and make a word list
2. Specify the URL and get the name of the website from it
3. Find the privacy policy automatically, or specify it
4. Scrape the website and save the HTML, text, sentences and words.
5. Search for all words in the list and save the records in a dataset.
6. Prepare the dataset and print it in a readable format.

# TESTING
- https://www.wolframalpha.com/ [OK]
- https://stackoverflow.com/ [OK]
- https://www.redditinc.com/ [OK]
- https://languagetool.org/ [OK]
- https://www.foxnews.com/ [OK]
- https://docs.github.com/ [OK]
- https://about.gitlab.com/ [ERR]

# TODOs
## New Features
- Filter required and optional attributes
- Named Entity Recognition (NLTK)
- Find data protection officer -> Problem: EMail protected
## Improve Features
- Improve detection of privacy policy (beautifoulsoup -> selenium)
- Reduce the error rate of accessible websites -> Problem: Unreadable Websites (gitlab)
- Improve the list from which attributes are drawn
- Improve recognition of headings and associated chapters


# Read policies
- https://docs.github.com/